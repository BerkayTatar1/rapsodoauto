package methods;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import runner.BaseTest;


public class Methods {

    private WebDriver webDriver;

    public Methods() {
        this.webDriver = BaseTest.driver;
    }


    public WebElement findElement(By by){
      return   webDriver.findElement(by);
    }


    public void click(By by){
        findElement(by).click();
    }

    public void sendKeys(By by,String text){
        findElement(by).sendKeys(text);
    }

    public String getText(By by){
       return findElement(by).getText();
    }

    public Boolean isEnabled(By by){
       return findElement(by).isEnabled();
    }

    public Boolean isDisplayed(By by){
       return findElement(by).isDisplayed();
    }


    public void jsClick(By by){
        WebElement element = findElement(by);
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", element);
    }


    public  void select(By by,String text){
        Select select = new Select(webDriver.findElement(by));
        select.selectByValue(text);
    }










}
