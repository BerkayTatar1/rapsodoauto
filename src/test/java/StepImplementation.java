import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import methods.Methods;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class StepImplementation {

    private Methods methods;

    public StepImplementation() {
        methods = new Methods();
    }


    private By diamondSportMenu = By.cssSelector("li a[title='DIAMOND SPORTS']");
    private By cookieAcceptBtn = By.id("cookie_action_close_header");
    private By sepetTutar = By.cssSelector("[class='wrap mr-5'] [href='https://rapsodo.com/cart/']");
    private By urun = By.cssSelector("[href='https://rapsodo.com/product/rapsodo-hitting-monitor/']");
    private By urunDetayEkranTitle = By.cssSelector("[class='product_title entry-title']");
    private By urunDetayEkranStokNo = By.cssSelector("[class='product_meta'] [class='sku']");
    private By addToCartDisable = By.cssSelector("[class*='variations_button woocommerce-variation-add-to-cart-disabled']");
    private By addToCartBtn = By.cssSelector("[name=\"add-to-cart\"]");
    private By selectItem = By.cssSelector("[id=\"stripe_plan_id\"]");
    private By closeBtn = By.cssSelector("[class=\"modal-content\"] [class=\"close-btn\"]");
    private By urunFiyatDetay = By.cssSelector("[class=\"single_variation_wrap\"] [class=\"price\"]");
    private By kuponKodu = By.cssSelector("[name=\"coupon_code\"]");
    private By alertMesaj = By.cssSelector("[class=\"alert alert-danger\"]");
    private By kuponOnaylaBtn = By.cssSelector("[class=\"actions\"] [name=\"apply_coupon\"]");


    @Step("DIAMOND SPORTS menüsünü aç")
    public void clickDiamondSportMenu() {
        methods.jsClick(diamondSportMenu);
    }


    @Step("Cookies Accept tıkla")
    public void cookieAcceptBtn() {
        methods.click(cookieAcceptBtn);
    }

    @Step("Sepet tutar : <text> kontrol")
    public void sepetTutar(String text) throws InterruptedException {
        Thread.sleep(5000);
        Assert.assertEquals(text, methods.getText(sepetTutar));
    }

    @Step("Urun Adı : <text> kontrol")
    public void urunDetayEkranTitle(String text) throws InterruptedException {
        Thread.sleep(5000);
        Assert.assertEquals(text, methods.getText(urunDetayEkranTitle));
    }

    @Step("Urun Detay ekranında Stok  No:<text> kontrol edilir")
    public void urunStokNo(String text) {
        Assert.assertEquals(text, methods.getText(urunDetayEkranStokNo));
    }


    @Step("Urun Detay ekranında Urun Fiyat: <text> kontrolü yapılır")
    public void urunFiyatDetayEkranKontrol(String text) throws InterruptedException {
        Thread.sleep(1000);
        Assert.assertEquals(text, methods.getText(urunFiyatDetay));
    }

    @Step("Hata mesajı Metni kontrolü yapılır")
    public void hataMesajKontrol() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println(methods.getText(alertMesaj));
        Assert.assertEquals("Coupon \"12312312\" does not exist!", methods.getText(alertMesaj));
    }

    @Step("Hitting 2.0 ürününe tıkla")
    public void urunTikla() {
        methods.click(urun);
    }


    @Step("addToCart butonuna tıkla")
    public void addToCartBtn() {
       methods.click(addToCartBtn);
    }


    @Step("addToCart buton enable olduğu kontrol edilir")
    public void addToCartIsEnabled() {
        Assert.assertTrue(methods.isEnabled(addToCartBtn));
    }

    @Step("Chose and option <text> değeri seç")
    public void selectPlan(String text) {
        methods.select(selectItem,text);
    }

    @Step("Popup close buton tıkla")
    public void closePopup() throws InterruptedException {
        Thread.sleep(2000);
        methods.click(closeBtn);
    }
    @Step("Kupon onayla butonu tıkla")
    public void kuponOnaylaBtn() throws InterruptedException {
        Thread.sleep(2000);
        methods.click(kuponOnaylaBtn);
    }

    @Step("Kupon kodu:<text> değeri doldur")
    public void kuponKoduDoldur(String text) throws InterruptedException {
        methods.sendKeys(kuponKodu,text);
    }


}
